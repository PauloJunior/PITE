/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConexaoBD;

import Modelos.Resposta;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import static javafx.scene.input.KeyCode.T;

/**
 *
 * @author Igor Mael
 */
public class Envio {
    
    public String Enviar(String endereco, String body) throws MalformedURLException, IOException {
            
        URL url = new URL(endereco);
        HttpURLConnection con = (HttpURLConnection)url.openConnection();  //connecting to url
        con.setRequestMethod("POST");
        con.setRequestProperty( "Accept", "*/*" );
        con.setRequestProperty( "Content-type", "application/json");
        con.setDoOutput(true);
        con.setRequestProperty("Accept-Charset", "UTF-8");
        con.setRequestProperty("charset", "UTF-8");
        
        try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
            byte[] mensagem = body.getBytes("UTF8");
            wr.write(mensagem, 0, mensagem.length);
            wr.flush();
            wr.close();
        }
        String json_response = "";
        InputStreamReader in = new InputStreamReader(con.getInputStream());
        BufferedReader br = new BufferedReader(in);
        String text = "";
        while ((text = br.readLine()) != null) {
            json_response += text;
        }
        return json_response;
    }
}

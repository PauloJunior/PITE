/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author Igor Mael
 */
public class Dependente {
    private Integer ID;
    private String create_at;
    private String updated_at;
    private String deleted_at;
    private String Titulo;
    private String NomeCompleto;
    private String Telefone;
    private String Rg;
    private String DataEmissao;
    private String Cpf;
    private String Parentesco;

    
    /**
     * @return the Titulo
     */
    public String getTitulo() {
        return Titulo;
    }

    /**
     * @param Titulo the Titulo to set
     */
    public void setTitulo(String Titulo) {
        this.Titulo = Titulo;
    }

    /**
     * @return the NomeCompleto
     */
    public String getNomeCompleto() {
        return NomeCompleto;
    }

    /**
     * @param NomeCompleto the NomeCompleto to set
     */
    public void setNomeCompleto(String NomeCompleto) {
        this.NomeCompleto = NomeCompleto;
    }

    /**
     * @return the Telefone
     */
    public String getTelefone() {
        return Telefone;
    }

    /**
     * @param Telefone the Telefone to set
     */
    public void setTelefone(String Telefone) {
        this.Telefone = Telefone;
    }

    /**
     * @return the RG
     */
    public String getRg() {
        return Rg;
    }

    /**
     * @param RG the RG to set
     */
    public void setRg(String Rg) {
        this.Rg = Rg;
    }

    /**
     * @return the DataEmissao
     */
    public String getDataEmissao() {
        return DataEmissao;
    }

    /**
     * @param DataEmissao the DataEmissao to set
     */
    public void setDataEmissao(String DataEmissao) {
        this.DataEmissao = DataEmissao;
    }

    /**
     * @return the Cpf
     */
    public String getCpf() {
        return Cpf;
    }

    /**
     * @param Cpf the Cpf to set
     */
    public void setCpf(String Cpf) {
        this.Cpf = Cpf;
    }

    /**
     * @return the Parentesco
     */
    public String getParentesco() {
        return Parentesco;
    }

    /**
     * @param Parentesco the Parentesco to set
     */
    public void setParentesco(String Parentesco) {
        this.Parentesco = Parentesco;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return ID;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.ID = id;
    }

    /**
     * @return the create_at
     */
    public String getCreate_at() {
        return create_at;
    }

    /**
     * @param create_at the create_at to set
     */
    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    /**
     * @return the updated_at
     */
    public String getUpdated_at() {
        return updated_at;
    }

    /**
     * @param updated_at the updated_at to set
     */
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    /**
     * @return the deleted_at
     */
    public String getDeleted_at() {
        return deleted_at;
    }

    /**
     * @param deleted_at the deleted_at to set
     */
    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }
    
    
    
}

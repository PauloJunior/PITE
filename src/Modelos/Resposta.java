/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

/**
 *
 * @author Igor Mael
 */
public class Resposta<T> {
   private String Erro;
   private List<T> Objeto = new ArrayList<T>();

    /**
     * @return the Erro
     */
    public String getErro() {
        return Erro;
    }

    /**
     * @param Erro the Erro to set
     */
    public void setErro(String Erro) {
        this.Erro = Erro;
    }

    /**
     * @return the Objeto
     */
    public List<T> getObjeto() {
        return Objeto;
    }

    /**
     * @param Objeto the Objeto to set
     */
    public void setObjeto(List<T> Objeto) {
        this.Objeto = Objeto;
    }

    /**
     * @return the Objeto
     */
   
    
}

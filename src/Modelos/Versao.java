/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author Igor Mael
 */
public class Versao {
    private double Versao;
    private int Release;
    private String Url;
    private String Changelog;

    public Versao(double Versao, int Release, String Url, String Changelog) {
        this.Versao = Versao;
        this.Release = Release;
        this.Url = Url;
        this.Changelog = Changelog;
    }
    
    public boolean Compara(Versao v) {
        return (this.getVersao() == v.getVersao()) && (this.getRelease() == v.getRelease());        
    }

    
    
    /**
     * @return the Versao
     */
    public double getVersao() {
        return Versao;
    }

    /**
     * @param Versao the Versao to set
     */
    public void setVersao(double Versao) {
        this.Versao = Versao;
    }

    /**
     * @return the Release
     */
    public int getRelease() {
        return Release;
    }

    /**
     * @param Release the Release to set
     */
    public void setRelease(int Release) {
        this.Release = Release;
    }

    /**
     * @return the Url
     */
    public String getUrl() {
        return Url;
    }

    /**
     * @param Url the Url to set
     */
    public void setUrl(String Url) {
        this.Url = Url;
    }

    /**
     * @return the Changelog
     */
    public String getChangelog() {
        return Changelog;
    }

    /**
     * @param Changelog the Changelog to set
     */
    public void setChangelog(String Changelog) {
        this.Changelog = Changelog;
    }
    
    
}

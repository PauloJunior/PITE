/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principal;
import ConexaoBD.Envio;
import Modelos.Resposta;
import Configuracoes.Propriedades;
import Modelos.Versao;
import TELAS.Login;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
/**
 *
 * @author Paulo
 */
public class Principal {
    
    
    public static void main(String args[]) throws IOException{
        
        Versao atualizarUrl =  existeAtualizacao();
        if (atualizarUrl == null){
            Login lg = new Login();
            lg.setVisible(true);
        }
        else {
            //JOptionPane.showOptionDialog(null, "Nova versão encontrada. Deseja atualizar?", atualizarUrl, 0, 0, icon, args, atualizarUrl)w(null, "ola");
            String Opcoes[] = new String[3];

            Opcoes[0] =  "Sim";
            Opcoes[1] = "Não";
            Opcoes[2] = "Changelog";
            
            int resp =JOptionPane.showOptionDialog(null, "Nova versão encontrada. Deseja atualizar?", "Atualização Encontrada", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE,null,Opcoes, 0);
            
            if (resp == 0) {
                Path nwdir = Files.createTempDirectory("PITE_");
                URL website = new URL(atualizarUrl.getUrl());
                try (InputStream in = website.openStream()) {
                Files.copy(in, nwdir.resolve("Setup.exe"), StandardCopyOption.REPLACE_EXISTING);
                File setup = new File(nwdir.toString()+"/Setup.exe");
                Desktop.getDesktop().open(setup);
                return;
}
            }
            if (resp == 1) {
                JOptionPane.showMessageDialog(null, "Aplicação não pode ser aberta.");
                return;
            }
            if (resp == 2) {
                JOptionPane.showMessageDialog(null, atualizarUrl.getChangelog());
            }
        }
    }
    
    public static Versao existeAtualizacao(){
        Envio envio = new Envio();
        Gson gson = new Gson();
        try {
            String resp = envio.Enviar(Propriedades.urlBase + "/sistema/versao", "");
            Resposta resposta = new Resposta();
            java.lang.reflect.Type Tipo = new TypeToken<Resposta<Versao>>() {}.getType();
            resposta = (Resposta) gson.fromJson(resp, Tipo);
            if (!resposta.getErro().isEmpty()) {
                JOptionPane.showMessageDialog(null, resposta.getErro());
                return null;
            }
            else {
                Versao versaoServidor = (Versao) resposta.getObjeto().get(0);
                System.out.println(versaoServidor.getUrl());
                if (versaoServidor.Compara(Propriedades.versao)) {
                    return null;
                }
                else {
                    return versaoServidor;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}

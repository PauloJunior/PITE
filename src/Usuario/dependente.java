/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuario;
/**
 *
 * @author Paulo
 */
public class dependente extends socio{
    
    //nTitulo é o mesmo dado que o do socio, por isso tem a extensão
    private String parentesco;
    private String nomeDep;
    private String telefoneDep;
    private String rgDep;
    private String dataEmissaoDep;
    private String cpfDep;
    
    

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    public String getNomeDep() {
        return nomeDep;
    }

    public void setNomeDep(String nomeDep) {
        this.nomeDep = nomeDep;
    }

    public String getTelefoneDep() {
        return telefoneDep;
    }

    public void setTelefoneDep(String telefoneDep) {
        this.telefoneDep = telefoneDep;
    }

    public String getRgDep() {
        return rgDep;
    }

    public void setRgDep(String rgDep) {
        this.rgDep = rgDep;
    }

    public String getDataEmissaoDep() {
        return dataEmissaoDep;
    }

    public void setDataEmissaoDep(String dataEmissaoDep) {
        this.dataEmissaoDep = dataEmissaoDep;
    }

    public String getCpfDep() {
        return cpfDep;
    }

    public void setCpfDep(String cpfDep) {
        this.cpfDep = cpfDep;
    }
}
